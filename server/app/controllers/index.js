var models  = require('../models');

var index ={
  /*Render Homepage*/
    time:function(req, res, next){
        models.sequelize.query("SELECT NOW();", { type: models.sequelize.QueryTypes.SELECT})
            .then(function(values) {
                console.log(values);
                res.json(values)
            })
    },
  home: function(req, res) {
  res.json({
    "name": "Air Quality API",
    "version":"0.1",
    "company":"Ajato GmBH",
    "author":"Fabian Glaus"
  });
}

};

module.exports = index;
