(function() {
    "use strict";

    angular
        .module("mt.services.api.co2")
        .factory("Co2Latest", ["$resource","Config",usecaseService])
        .factory("Co2Statistics",["$resource","Config",tempStatistik])
        .factory("Co2StatisticsDay",["$resource","Config",tempStatistikDay]);

    /**
     * Auth service, handles simple login
     */
    function usecaseService($resource,Config) {
        return $resource(Config.apiUrl+'/co2/latest',{},{
            'get':{
                method:'GET'
            }
        });
    }

    function tempStatistik($resource,Config) {
        return $resource(Config.apiUrl+'/co2/statistics/hour',{},{
            'get':{
                method:'GET',
                isArray:true
            }
        });
    }

    function tempStatistikDay($resource,Config) {
        return $resource(Config.apiUrl+'/co2/statistics/day',{},{
            'get':{
                method:'GET',
                isArray:true
            }
        });
    }

})();