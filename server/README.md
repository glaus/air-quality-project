# APP Info
Version: 1.5

# Required Software

Docker: https://www.docker.com/products/overview
Node JS: https://nodejs.org/en/
Sequelize: `npm install -g sequelize-cli`

# Run the App with Docker
1. Setup the Database:
`docker run --name mysql -e MYSQL_ROOT_PASSWORD=secret123 -p 3306:3306 -d mysql`
2. Build The Application 
`docker build -t air-quality:1.5 .`
3. Save the Application 
`docker save air-quality:1.5 > release/air-quality-1.5.tar`
3. Have Fun with the Container

# Dev
1. Setup the Database:
`docker run --name mysql -e MYSQL_ROOT_PASSWORD=secret123 -p 3306:3306 -d mysql`
2. Adjust the Config
`app/config/config.js` 
3. Start the App:
`npm start` 

# Build Docker
`docker save mt-crem:latest > release/mt-crem-v-0.1.tar`

# Usage

