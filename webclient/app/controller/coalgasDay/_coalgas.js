(function() {
    "use strict";

    angular
        .module("mt.controller.coalgasDay",[])
        .config(usecaseRouteProvider);

    function usecaseRouteProvider($stateProvider) {
        $stateProvider
            .state("coalgasDay", {
                views: {
                    "mt-content" : {
                        templateUrl: "coalgasDay.jade",
                        controller: "CoalGasDayController"
                    },
                    "mt-topbar":{
                        templateUrl: "topbar.jade",
                        controller:"TopbarController"
                    }
                },
                parent:"mt",
                url:"/coalgas/day"
            });
    }

})();
