var models  = require('../models');
var Sequelize = require('sequelize');

var user ={

    /**
     * Returns a List of User
     * @param req
     * offset -> int
     * limit -> int
     * @param res
     */
    list: function(req, res) {
        models.User.findAndCountAll({
            attributes: { exclude: ['password_hash','updatedAt','createdAt','deletedAt'] },
            offset: Number(req.query.offset)||0,
            limit: Number(req.query.limit)||10
        }).then(function(users) {
            res.json(users)
        });
    },

    /**
     * Returns a Signel entry by ID
     * @param req
     * id -> int
     * @param res
     * @param next
     */
    get: function(req, res, next) {
        models.User.findOne({
            attributes: { exclude: ['password_hash','deletedAt'] },
            where:{
                id: req.params.id
            }
        }).then(function(user) {
            user ? res.json(user) : next(user);
        });
    },

    /**
     * Add a User
     * @param req
     * Body:
     * firstname -> string (optional)
     * lastname -> string (optional)
     * email -> email Address
     * password -> string
     * @param res
     */
    add: function(req, res) {
        models.User.create(req.body).then(function(user) {
            user ? res.json(user) : next();
        }).catch(Sequelize.ValidationError, function (err) {
            return res.status(422).send(err.errors);
        }).catch(function (err) {
            return res.status(400).send({
                message: err.message
            });
        });
    },

    /**
     * Updates a User
     * @param req
     * id -> int
     * firstname -> string (optional)
     * lastname -> string (optional)
     * email -> string (optional)
     * password-> string (optional)
     * @param res
     * @param next
     */
    update: function(req, res, next) {
        models.User.findById(req.params.id).then(function(user) {
            if (user) {
                user.updateAttributes(req.body).then(function(status) {
                    res.json(status)
                }).catch(Sequelize.ValidationError, function (err) {
                    return res.status(422).send(err.errors);
                }).catch(function (err) {
                    return res.status(400).send({
                        message: err.message
                    });
                });
            }else{
                next()
            }
        })
    },

    /**
     * Deletes a single Entry
     * @param req
     * @param res
     * @param next
     */
    delete: function(req, res) {
        models.User.destroy({
            where: {
                id:  req.params.id
            }
        }).then(function(status){
            status ? res.json({
                success: true
            }) : res.status(400).send({
                message: "Could not delete User"
            });
        });
    },
    linkUseCase: function(req, res, next) {
        models.Use_case.findById(req.params.id_use_case).then(function(useCase) {
            if (useCase) {
                useCase.updateAttributes({
                    fi_user:req.params.id_user
                }).then(function(status) {
                    res.json(status)
                }).catch(Sequelize.ValidationError, function (err) {
                    return res.status(422).send(err.errors);
                }).catch(function (err) {
                    return res.status(400).send({
                        message: err.message
                    });
                });
            }else{
                next()
            }
        })
    },
    unlinkUseCase: function(req, res, next) {
        models.Use_case.findById(req.params.id_use_case).then(function(useCase) {
            if (useCase) {
                useCase.updateAttributes({
                    fi_user:null
                }).then(function(status) {
                    res.json(status)
                }).catch(Sequelize.ValidationError, function (err) {
                    return res.status(422).send(err.errors);
                }).catch(function (err) {
                    return res.status(400).send({
                        message: err.message
                    });
                });
            }else{
                next()
            }
        })
    }



};

module.exports = user;