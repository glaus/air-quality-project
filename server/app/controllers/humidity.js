var models  = require('../models');
var Sequelize = require('sequelize');

var useCase ={

    /**
     * Returns a List of User
     * @param req
     * offset -> int
     * limit -> int
     * @param res
     */
    list: function(req, res) {
        models.Humidity.findAndCountAll({
            attributes: { exclude: ['updatedAt','createdAt'] },
            offset: Number(req.query.offset)||0,
            limit: Number(req.query.limit)||10
        }).then(function(resSql) {
            res.json(resSql)
        });
    },

    /**
     * Returns a Signel entry by ID
     * @param req
     * id -> int
     * @param res
     * @param next
     */
    get: function(req, res, next) {
        models.Humidity.findOne({
            attributes: { exclude: [] },
            where:{
                id: req.params.id
            }
        }).then(function(resSql) {
            resSql ? res.json(resSql) : next(resSql);
        });
    },

    getLatest: function(req, res, next){
        models.Humidity.findOne({
            attributes: { exclude: [] },
            order:[
                ['createdAt','DESC']
            ],
            limit:1
        }).then(function(resSql) {
            resSql ? res.json(resSql) : next(resSql);
        });
    },
    getStatisticHour:function(req, res, next){
        models.sequelize.query("SELECT AVG(`value`)AS val,MINUTE(`createdAt`) AS hour FROM Humidity WHERE HOUR(`createdAt`) = HOUR(NOW()+ INTERVAL ".concat((process.env.TIME_OFFSET_HOUR === undefined) ? "1" : String(process.env.TIME_OFFSET_HOUR)," HOUR) AND DATE(createdAt+ INTERVAL ",(process.env.TIME_OFFSET_DAY === undefined) ? "1" : String(process.env.TIME_OFFSET_DAY)," DAY ) = Curdate() GROUP BY MINUTE(`createdAt`);"), { type: models.sequelize.QueryTypes.SELECT})
            .then(function(values) {
                res.json(values)
            })
    },
    getStatisticDay:function(req, res, next){
        models.sequelize.query("SELECT AVG(`value`)AS val,HOUR(`createdAt`) AS hour FROM Humidity WHERE DATE(`createdAt`) = CURDATE() GROUP BY HOUR(`createdAt`)", { type: models.sequelize.QueryTypes.SELECT})
            .then(function(values) {
                res.json(values)
            })
    },

    /**
     * Add a User
     * @param req
     * Body:
     * firstname -> string (optional)
     * lastname -> string (optional)
     * email -> email Address
     * password -> string
     * @param res
     */
    addParam: function(req, res){
      var body =  {
          value:req.params.value
      };
      models.Humidity.create(body).then(function(resSql) {
          resSql ? res.json(resSql) : next();
      }).catch(Sequelize.ValidationError, function (err) {
          return res.status(422).send(err.errors);
      }).catch(function (err) {
          return res.status(400).send({
              message: err.message
          });
      });
    },


    add: function(req, res) {
        models.Humidity.create(req.body).then(function(resSql) {
            resSql ? res.json(resSql) : next();
        }).catch(Sequelize.ValidationError, function (err) {
            return res.status(422).send(err.errors);
        }).catch(function (err) {
            return res.status(400).send({
                message: err.message
            });
        });
    },

    /**
     * Updates a User
     * @param req
     * id -> int
     * firstname -> string (optional)
     * lastname -> string (optional)
     * email -> string (optional)
     * password-> string (optional)
     * @param res
     * @param next
     */
    update: function(req, res, next) {
        models.Humidity.findById(req.params.id).then(function(resSql) {
            if (resSql) {
                resSql.updateAttributes(req.body).then(function(status) {
                    res.json(status)
                }).catch(Sequelize.ValidationError, function (err) {
                    return res.status(422).send(err.errors);
                }).catch(function (err) {
                    return res.status(400).send({
                        message: err.message
                    });
                });
            }else{
                next()
            }
        })
    },

    /**
     * Deletes a single Entry
     * @param req
     * @param res
     * @param next
     */
    delete: function(req, res) {
        models.Humidity.destroy({
            where: {
                id:  req.params.id
            }
        }).then(function(status){
            status ? res.json({
                success: true
            }) : res.status(400).send({
                message: "Could not delete User"
            });
        });
    },



};

module.exports = useCase;