(function() {
    "use strict";

    angular
        .module("mt.services.api.temperature")
        .factory("TemperatureLatest", ["$resource","Config",usecaseService])
        .factory("TemperatureStatisticsDay",["$resource","Config",tempStatistikDay])
        .factory("TemperatureStatistics",["$resource","Config",tempStatistik]);

    /**
     * Auth service, handles simple login
     */
    function usecaseService($resource,Config) {
        return $resource(Config.apiUrl+'/temperature/latest',{},{
            'get':{
                method:'GET'
            }
        });
    }

    function tempStatistik($resource,Config) {
        return $resource(Config.apiUrl+'/temperature/statistics/hour',{},{
            'get':{
                method:'GET',
                isArray:true
            }
        });
    }

    function tempStatistikDay($resource,Config) {
        return $resource(Config.apiUrl+'/temperature/statistics/day',{},{
            'get':{
                method:'GET',
                isArray:true
            }
        });
    }

})();