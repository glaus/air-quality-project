var jwt = require('jsonwebtoken');
var models  = require('../models');
var moment = require('moment');
var bcrypt = require('bcrypt');
var config = require('../config/config.js');

var auth = {

    login: function(req, res) {

        var email = req.body.email || '';
        var password = req.body.password || '';

        if (email == '' || password == '') {
            invalidCredentials(res);
            return;
        }

        // Fire a query to your DB and check if the credentials are valid
        models.User.findOne({
            attributes: [ 'id','email','password_hash'] ,
            where:{
                email: email
            }
        }).then(function(user) {
            if (user){
                pwValid = bcrypt.compareSync(password, user.password_hash);
                pwValid ? res.json(genToken(user)):invalidCredentials(res);
            }else{
                invalidCredentials(res);
            }
        });
    }
};


function invalidCredentials(res){
    res.status(401);
    res.json({
        "status": 401,
        "message": "Invalid Credentials"
    });
}

// private method
function genToken(user) {
    var expires = 24;
    var token = jwt.sign({ user: user }, config.secret,{
        expiresIn:expires+"h"
    });

    return {
        token: token,
        expires: moment().add(expires, 'hours').format()
    };
}


module.exports = auth;