


(function() {
    "use strict";

    angular
        .module("mt.controller.lightlevelDay",[])
        .config(usecaseRouteProvider);

    function usecaseRouteProvider($stateProvider) {
        $stateProvider
            .state("lightlevelDay", {
                views: {
                    "mt-content" : {
                        templateUrl: "lightlevelDay.jade",
                        controller: "LightLevelDayController"
                    },
                    "mt-topbar":{
                        templateUrl: "topbar.jade",
                        controller:"TopbarController"
                    }
                },
                parent:"mt",
                url:"/lightlevel/Day"
            });
    }

})();
