(function() {
    "use strict";

    angular
        .module("mt.controller.co2Day",[])
        .config(usecaseRouteProvider);

    function usecaseRouteProvider($stateProvider) {
        $stateProvider
            .state("co2Day", {
                views: {
                    "mt-content" : {
                        templateUrl: "co2Day.jade",
                        controller: "Co2DayController"
                    },
                    "mt-topbar":{
                        templateUrl: "topbar.jade",
                        controller:"TopbarController"
                    }
                },
                parent:"mt",
                url:"/co2"
            });
    }

})();
