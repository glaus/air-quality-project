(function() {
    "use strict";

    angular
        .module("mt.controller.overview",[])
        .config(usecaseRouteProvider);

    function usecaseRouteProvider($stateProvider) {
        $stateProvider
            .state("overview", {
                views: {
                    "mt-content" : {
                        templateUrl: "overview.jade",
                        controller: "OverviewController"
                    },
                    "mt-topbar" : {
                        templateUrl: "topbar.jade",
                        controller: "TopbarController"
                    },
                },
                parent:"mt",
                url:"/overview"
            });
    }

})();
