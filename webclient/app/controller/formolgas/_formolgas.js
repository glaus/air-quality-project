


(function() {
    "use strict";

    angular
        .module("mt.controller.formolgas",[])
        .config(usecaseRouteProvider);

    function usecaseRouteProvider($stateProvider) {
        $stateProvider
            .state("formolgas", {
                views: {
                    "mt-content" : {
                        templateUrl: "formolgas.jade",
                        controller: "FormolgasController"
                    },
                    "mt-topbar":{
                        templateUrl: "topbar.jade",
                        controller:"TopbarController"
                    }
                },
                parent:"mt",
                url:"/formolgas"
            });
    }

})();
