(function() {
    "use strict";

    angular
        .module("mt.controller.humidity",[])
        .config(usecaseRouteProvider);

    function usecaseRouteProvider($stateProvider) {
        $stateProvider
            .state("humidity", {
                views: {
                    "mt-content" : {
                        templateUrl: "humidity.jade",
                        controller: "HumidityController"
                    },
                    "mt-topbar":{
                        templateUrl: "topbar.jade",
                        controller:"TopbarController"
                    }
                },
                parent:"mt",
                url:"/humidity"
            });
    }

})();
