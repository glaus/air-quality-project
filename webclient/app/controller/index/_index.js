(function() {
    "use strict";

    angular
        .module("mt.controller.index",[])
        .config(timetableRouteProvider);

    function timetableRouteProvider($stateProvider) {
        $stateProvider
            .state("index", {
                parent:"mt",
                views: {
                    "mt-content" : {
                        controller: "IndexController"
                    }
                },
                url:""

            });
    }

})();