/*jslint node: true */
"use strict";


module.exports = function(grunt) {

    //Load all tasks
    require('load-grunt-tasks')(grunt);

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        /*
        * Sample Page
        */
        jade: {
            options: {
                pretty: false,
                doctype: "html",
                data: {
                    debug: false
                }
            },
            app:{
                files: {
                    "dist/index.html": "app/index.jade"
                }
            }
        },

        /*
         * --------HTML Templates in templates.js--------
         */
        html2js: {
            options: {
                base: "app",
                module: "mt.templates",
                useStrict: true,
                rename: function (moduleName) {
                    return moduleName.substring(moduleName.lastIndexOf('/')+1);
                },
                htmlmin: {
                    collapseBooleanAttributes: true,
                    collapseWhitespace: true,
                    removeAttributeQuotes: true,
                    removeComments: true,
                    removeEmptyAttributes: true,
                    removeRedundantAttributes: true,
                    removeScriptTypeAttributes: true,
                    removeStyleLinkTypeAttributes: true
                }
            },
            app: {
                src: [
                    "app/**/*.html",
                    "app/**/*.jade",
                    "app/**/*.htm"
                ],
                dest: "temp/concat/templates.js"
            }
        },


        /*
         * ---------------Libs-------------
        */

        bower_concat: {
            app: {
                dest: 'temp/concat/lib.js'
            }
        },


        /*
         * ---------------CSS-------------
         */
        less_imports: {
            options: {
                inlineCSS: false
            },
            app: {
                src: [
                    'app/**/*.less',
                    "components/**/*.less"
                ],
                dest: 'temp/less/imports.less'
            }
        },
        less: {
            app: {
                options: {
                    compress: false
                },
                files: {
                    "temp/rt.css": "temp/less/imports.less" // destination file and source file
                }
            }
        },

        /*
         * ---------------App.js-----------
         */
        ngAnnotate: {
            app:{
                src: [
                    "app/**/*.js",
                    "components/**/*.js"
                ],
                dest: "temp/concat/app.js"
            }
        },


        /*
         * --------- Helper -----------
         */
        concat: {
            dist: {
                src: ['temp/concat/lib.js', 'temp/concat/templates.js','temp/concat/app.js'],
                dest: 'dist/rt.js'
            }
        },

        uglify: {
            app: {
                files: {

                    'dist/rt.min.js': ['dist/rt.js']
                }
            },
            options: {
                banner: "/*! <%= pkg.name %> <%= grunt.template.today(\"yyyy-mm-dd\") %> */\n",
                mangle: true
            }
        },

        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1
            },
            app: {
                files: {
                    'dist/rt.min.css': 'dist/rt.css'
                }
            }
        },

        clean: {
            temp: {
                src:["temp/"]
            },
            dist:{
                src:["dist/"]
            }
        },

        copy: {
            app:{
                files: [
                    {expand: true, cwd:"temp/",src: ['rt.css'], dest: 'dist/'},
                    {expand: true, cwd:"bower_components/bootstrap/fonts",src: ['*'], dest: 'dist/fonts'}
                ]
            }
        },

        connect: {
            app: {
                options: {
                    port: 8080,
                    base: 'dist/'
                }
            }
        },

        asciify: {
            build: {
                text: "Build Completed"
            },
            ship: {
                text: "Ship It"
            },
            ready: {
                text: "App Ready"
            },
            changed: {
                text: "App Changed"
            },
            options: {
                font: "starwars",
                log:true
            }
        },

        watch: {
            options: {
                livereload: true,
                atBegin: false
            },
            app:{
                files: [
                    "app/**/*.html","app/**/*.jade",
                    "app/**/*.js",
                    "app/**/*.json",
                    "app/**/*.less"
                ],
                tasks: [
                    "build",
                    "asciify:changed"
                ]
            }
        }

    });


    // Default task
    grunt.registerTask("default", [
        "build",
        "connect:app",
        "asciify:ready",
        "watch:app"
    ]);

    grunt.registerTask("ship",[
        "build",
        "uglify:app",
        "cssmin:app",
        "asciify:ship"
    ]);


    grunt.registerTask("build",[
        //Prepare Folders
        "clean:dist",
        "clean:temp",
        //Sample App
        "jade:app",
        //Generate Files
        "html2js:app",
        "ngAnnotate:app",
        "less_imports:app",
        "less:app",
        "bower_concat:app",
        //Copy to Dist
        "copy:app",
        "concat:dist",
        "clean:temp",
        "asciify:build"
    ]);


};
