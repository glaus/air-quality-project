'use strict';
module.exports = function(sequelize, DataTypes) {
    var Temperature = sequelize.define('Dust', {
        value: {
            type: DataTypes.FLOAT
        }
    }, {
        freezeTableName: true,
        paranoid: false,
        classMethods: {
            associate: function(models) {

            }
        }
    });
    return Temperature;
};