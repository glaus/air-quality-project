(function() {
  "use strict";

  angular
    .module("mt.controller", [
        "mt.controller.topbar",
        "mt.controller.index",
        "mt.controller.overview",
        "mt.controller.temperature",
        "mt.controller.coalgas",
        "mt.controller.coalgasDay",
        "mt.controller.lightlevel",
        "mt.controller.lightlevelDay",
        "mt.controller.dust",
        "mt.controller.dustDay",
        "mt.controller.formolgas",
        "mt.controller.formolgasDay",
        "mt.controller.humidity",
        "mt.controller.humidityDay",
        "mt.controller.co2",
        "mt.controller.co2Day",
        "mt.controller.temperatureDay"
    ]).config(showRouteProvider);

    function showRouteProvider($stateProvider) {
        $stateProvider
            .state("mt", {
                abstract:true,
                views: {
                    "mt-main":{
                        templateUrl: "controller.jade"
                    }
                }
            });
    }

})();