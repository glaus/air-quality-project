(function() {
    "use strict";

    angular
        .module("mt.services.api.coalgas")
        .factory("CoalGasLatest", ["$resource","Config",usecaseService])
        .factory("CoalGasStatistics",["$resource","Config",tempStatistik])
        .factory("CoalGasStatisticsDay",["$resource","Config",tempStatistikDay]);

    /**
     * Auth service, handles simple login
     */
    function usecaseService($resource,Config) {
        return $resource(Config.apiUrl+'/coalgas/latest',{},{
            'get':{
                method:'GET'
            }
        });
    }

    function tempStatistik($resource,Config) {
        return $resource(Config.apiUrl+'/coalgas/statistics/hour',{},{
            'get':{
                method:'GET',
                isArray:true
            }
        });
    }

    function tempStatistikDay($resource,Config) {
        return $resource(Config.apiUrl+'/coalgas/statistics/day',{},{
            'get':{
                method:'GET',
                isArray:true
            }
        });
    }

})();