'use strict';
var bcrypt = require('bcrypt');
const saltRounds = 10;


module.exports = function(sequelize, DataTypes) {
  var User = sequelize.define('User', {
    firstname: {
      type: DataTypes.STRING,
      validate:{
        is: ["^[a-z]+$",'i'],
        len: [2,35]
      }
    },
    lastname: {
      type: DataTypes.STRING,
      validate:{
        is: ["^[a-z]+$",'i'],
        len: [2,35]
      }
    },
    email: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
      validate:{
        isEmail: true
      }
    },
    password_hash: DataTypes.STRING,
    password: {
      type: DataTypes.VIRTUAL,
      allowNull: false,
      validate: {
        len: [8,100]
      },
      set: function (val) {
        this.setDataValue('password', val); // Remember to set the data value, otherwise it won't be validated
        this.setDataValue('password_hash', bcrypt.hashSync(val, saltRounds));
      }
    }
  }, {
      paranoid: true,
      freezeTableName: true,
      classMethods: {
        associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return User;
};