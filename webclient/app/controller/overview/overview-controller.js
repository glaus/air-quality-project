(function() {
    "use strict";

    angular.module("mt.controller.overview")
        .controller("OverviewController", ["$scope","$state","$interval","TemperatureLatest","LightLevelLatest","CoalGasLatest","Co2Latest","DustLatest","FormolgasLatest","HumidityLatest",overviewController]);

    function overviewController($scope, $state,$interval, TemperatureLatest,LightLevelLatest,CoalGasLatest, Co2Latest,DustLatest, FormolgasLatest, HumidityLatest) {
        getValues();
        $interval(function () {
            getValues();
        }, 10000);


        function getValues(){
            getCoalGas();
            getTemperature();
            getLightLevel();
            getDust();
            getHumidity();
            getFormolgas();
            getCo2();
        }


        function getCoalGas(){
            CoalGasLatest.get({}, function(res){
                var date = new Date(res.createdAt);
                res.createdAt = date.toString();
                $scope.coalgas = res;
            },function(err){
                console.log(err)
            });
        }

        $scope.getCoalGasCss =  function(){
            if ($scope.coalgas && $scope.coalgas.value < 0.8){
                return "thumbnail-ok"
            }else if ($scope.coalgas && $scope.coalgas.value > 0.9){
                return "thumbnail-warning"
            }else{
                return ""
            }
        };

        function getCo2(){
            Co2Latest.get({}, function(res){
                var date = new Date(res.createdAt);
                res.createdAt = date.toString();
                $scope.co2 = res;
            },function(err){
                console.log(err)
            });
        }

        $scope.getCo2Css =  function(){
            if ($scope.co2 && $scope.co2.value < 500){
                return "thumbnail-ok"
            }else if ($scope.co2 && $scope.co2.value > 700){
                return "thumbnail-warning"
            }else{
                return ""
            }
        };


        function getDust(){
            DustLatest.get({}, function(res){
                var date = new Date(res.createdAt);
                res.createdAt = date.toString();
                $scope.dust = res;
            },function(err){
                console.log(err)
            });
        }

        $scope.getDustCss =  function(){
            if ($scope.dust && $scope.dust.value < 0.8){
                return "thumbnail-ok"
            }else if ($scope.dust && $scope.dust.value > 1.1){
                return "thumbnail-warning"
            }else{
                return ""
            }
        };

        function getFormolgas(){
            FormolgasLatest.get({}, function(res){
                var date = new Date(res.createdAt);
                res.createdAt = date.toString();
                $scope.formolgas = res;
            },function(err){
                console.log(err)
            });
        }

        $scope.getFormolgasCss =  function(){
            if ($scope.formolgas && $scope.formolgas.value < 3){
                return "thumbnail-ok"
            }else if ($scope.formolgas && $scope.formolgas.value > 4){
                return "thumbnail-warning"
            }else{
                return ""
            }
        };

        function getHumidity(){
            HumidityLatest.get({}, function(res){
                var date = new Date(res.createdAt);
                res.createdAt = date.toString();
                $scope.humidity = res;
            },function(err){
                console.log(err)
            });
        }

        $scope.getHumidityCss =  function(){
            if ($scope.humidity && $scope.humidity.value < 30){
                return "thumbnail-warning"
            }else if ($scope.humidity && $scope.humidity.value > 70){
                return "thumbnail-warning"
            }else{
                return "thumbnail-ok"
            }
        };

        function getLightLevel(){
            LightLevelLatest.get({}, function(res){
                var date = new Date(res.createdAt);
                res.createdAt = date.toString();
                $scope.lightlevel = res;
            },function(err){
                console.log(err)
            });
        }

        $scope.getLightLevelCss =  function(){
            if ($scope.lightlevel && $scope.lightlevel.value > 650){
                return "thumbnail-ok"
            }else if ($scope.lightlevel && $scope.lightlevel.value < 350){
                return "thumbnail-warning"
            }else{
                return ""
            }
        };

        function getTemperature(){
            TemperatureLatest.get({}, function(res){
                var date = new Date(res.createdAt);
                res.createdAt = date.toString();
                $scope.temperature = res;
            },function(err){
                console.log(err)
            });
        }
        $scope.getTemperatureCss =  function(){
            if ($scope.temperature && $scope.temperature.value > 19){
                return "thumbnail-ok"
            }else if ($scope.temperature && $scope.temperature.value < 10){
                return "thumbnail-warning"
            }else{
                return ""
            }
        };

    }

})();
