(function() {
    "use strict";

    angular
        .module("mt.controller.dust",[])
        .config(usecaseRouteProvider);

    function usecaseRouteProvider($stateProvider) {
        $stateProvider
            .state("dust", {
                views: {
                    "mt-content" : {
                        templateUrl: "dust.jade",
                        controller: "DustController"
                    },
                    "mt-topbar":{
                        templateUrl: "topbar.jade",
                        controller:"TopbarController"
                    }
                },
                parent:"mt",
                url:"/dust"
            });
    }

})();
