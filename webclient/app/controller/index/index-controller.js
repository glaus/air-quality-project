(function() {
    "use strict";

    angular.module("mt.controller.index")
        .controller("IndexController", ["$state",indexController]);

    function indexController($state) {
        $state.go("overview");
    }

})();