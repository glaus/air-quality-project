'use strict';

const http = require('http');
const express = require('express');
const path = require('path');
const logger = require('morgan');
const bodyParser = require('body-parser');
var cors = require('cors');

const config = require('./config/config.js');


// Constants
const PORT = 9000;

// App
const app = express();
app.use(logger('short'));
app.use(cors({
    "origin": "*",
    "methods": "GET,HEAD,PUT,POST,DELETE",
    "preflightContinue": true
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/', require('./routes'));


/**
 * Error Handling
 */

//404 Error handler
app.use(function(req, res, next) {
  res.status(404);
  res.json({
    status: 404,
    message: "Page not Found"
  });
});

// 500 development error handler
if (app.get('env') === 'development') {
  app.use(function(err, req, res) {
    res.status(err.status || 500);
    res.json({
      message: err.message,
      error: err
    });
  });
}

// 500 production error handler
app.use(function(err, req, res) {
  res.status(err.status || 500);
  res.json({
    message: err.message,
    error: {}
  });
});


app.get('/', function (req, res) {
  res.send('Hello world\n');
});


/**
 * Startup Server
 */

app.set('port', PORT);
var server = http.createServer(app);

server.listen(PORT);
console.log('Running on http://localhost:' + PORT);
