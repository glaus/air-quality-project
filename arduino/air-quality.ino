#include <Wire.h>
#include <UnoWiFiDevEd.h>
#include <math.h>

//Pins Analog
const int mq9Pin = A0;
const int tempPin = A1;
const int lightLevelPin = A2;
const int hch0Pin = A3;

//Pins Digital
const int dustPin = 8;
unsigned long duration;
unsigned long sampletime_ms = 2000;//sampe 30s&nbsp;;
unsigned long lowpulseoccupancy = 0;
float ratio = 0;
float concentration = 0;
unsigned long starttime;

void setup() {
  Serial.begin(9600);
  //Dust Sensor
  pinMode(dustPin,INPUT);
  starttime = millis();
  //Connect Server
  Serial.println("Started");
  Ciao.begin();
}

void loop() {
  Serial.println("Requests Started");
  float temperature = readTemperature();
  int lightlevel = readLightLevel();
  float mq9 = readMQ9();
  float dust = readDust();
  float hch0 = readHch0();
  Serial.println(hch0);
  doRequests(temperature,mq9,lightlevel, dust, hch0);
  Serial.println("Requests done");
}

void doRequests(float temperature,float mq9,int lightlevel, float dust,float hch0){
  httpRequest("/temperature/"+String(temperature), "POST");
  httpRequest("/dust/"+String(dust), "POST");
  httpRequest("/coalgas/"+String(mq9), "POST");
  httpRequest("/lightlevel/"+String(lightlevel), "POST");
  httpRequest("/formolgas/"+String(hch0), "POST");
}


