(function() {
    "use strict";

    angular
        .module("mt.controller.temperature",[])
        .config(usecaseRouteProvider);

    function usecaseRouteProvider($stateProvider) {
        $stateProvider
            .state("temperature", {
                views: {
                    "mt-content" : {
                        templateUrl: "temperature.jade",
                        controller: "TemperatureController"
                    },
                    "mt-topbar":{
                        templateUrl: "topbar.jade",
                        controller:"TopbarController"
                    }
                },
                parent:"mt",
                url:"/temperature"
            });
    }

})();
