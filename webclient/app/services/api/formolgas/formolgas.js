
(function() {
    "use strict";

    angular
        .module("mt.services.api.formolgas")
        .factory("FormolgasLatest", ["$resource","Config",usecaseService])
        .factory("FormolgasStatisticsDay",["$resource","Config",tempStatistikDay])
        .factory("FormolgasStatistics",["$resource","Config",tempStatistik]);

    /**
     * Auth service, handles simple login
     */
    function usecaseService($resource,Config) {
        return $resource(Config.apiUrl+'/formolgas/latest',{},{
            'get':{
                method:'GET'
            }
        });
    }

    function tempStatistik($resource,Config) {
        return $resource(Config.apiUrl+'/formolgas/statistics/hour',{},{
            'get':{
                method:'GET',
                isArray:true
            }
        });
    }

    function tempStatistikDay($resource,Config) {
        return $resource(Config.apiUrl+'/formolgas/statistics/day',{},{
            'get':{
                method:'GET',
                isArray:true
            }
        });
    }

})();