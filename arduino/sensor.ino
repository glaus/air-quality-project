/*
---------Input Sensors-------------
*/


/*
 * Temperature Sensor
 * Returns the temperature in C°
 */
float readTemperature(){
  int a = analogRead(tempPin);
  float R = 1023.0/((float)a)-1.0;R = 100000.0*R;
  return 1.0/(log(R/100000.0)/4275+1/298.15)-273.15;//convert to temperature via datasheet ;
}

/*
 * Light Level Sensor
 */
int readLightLevel(){
  return int(analogRead(lightLevelPin));; 
}

/*
 * MQ 9 Gas Sensor
 * Returns the Intencity of coal gas per mg/L
 */
float readMQ9(){
  int sensorValue = analogRead(mq9Pin);
  return (float)sensorValue/2048*25.0;
}

/*
 * HCH0 Sensor
 * Return the Intencity of Formol Gas per mg/m^
 */
float readHch0(){
  int sensorValue= analogRead(hch0Pin);
  return (float)sensorValue/1024*1.2;
}


/*
 * Dust Sensor
 * Returns the Dust in 
 */

 float readDust(){
  duration = pulseIn(dustPin, LOW);
  lowpulseoccupancy = lowpulseoccupancy+duration;

  if ((millis()-starttime) >= sampletime_ms)//if the sampel time = = 30s
  {
    ratio = lowpulseoccupancy/(sampletime_ms*10.0);  // Integer percentage 0=&gt;100
    concentration = 1.1*pow(ratio,3)-3.8*pow(ratio,2)+520*ratio+0.62; // using spec sheet curve
    lowpulseoccupancy = 0;
    starttime = millis();
    return concentration;
  }else{
    return 0;
  }
 }


