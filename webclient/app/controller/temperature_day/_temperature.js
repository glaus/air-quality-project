(function() {
    "use strict";

    angular
        .module("mt.controller.temperatureDay",[])
        .config(usecaseRouteProvider);

    function usecaseRouteProvider($stateProvider) {
        $stateProvider
            .state("temperatureDay", {
                views: {
                    "mt-content" : {
                        templateUrl: "temperatureDay.jade",
                        controller: "TemperatureDayController"
                    },
                    "mt-topbar":{
                        templateUrl: "topbar.jade",
                        controller:"TopbarController"
                    }
                },
                parent:"mt",
                url:"/temperature/day"
            });
    }

})();
