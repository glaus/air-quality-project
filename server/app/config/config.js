module.exports = {

    'secret': 'ilovescotchyscotch',

    //Databases

    "development": {
        "username": (process.env.DB_USER === undefined) ? "root" : String(process.env.DB_USER),
        "password": (process.env.DB_PASSWORD === undefined) ? "secret123" : String(process.env.DB_PASSWORD),
        "database": (process.env.DB_NAME === undefined) ? "tin_air_quality_prod" : String(process.env.DB_NAME),
        "host": (process.env.DB_HOST === undefined) ? "0.0.0.0" : String(process.env.DB_HOST),
        "port":(process.env.DB_PORT === undefined) ? "3306" : String(process.env.DB_PORT),
        "dialect": "mysql"
    }
};