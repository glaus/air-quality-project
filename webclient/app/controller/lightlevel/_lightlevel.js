


(function() {
    "use strict";

    angular
        .module("mt.controller.lightlevel",[])
        .config(usecaseRouteProvider);

    function usecaseRouteProvider($stateProvider) {
        $stateProvider
            .state("lightlevel", {
                views: {
                    "mt-content" : {
                        templateUrl: "lightlevel.jade",
                        controller: "LightLevelController"
                    },
                    "mt-topbar":{
                        templateUrl: "topbar.jade",
                        controller:"TopbarController"
                    }
                },
                parent:"mt",
                url:"/lightlevel"
            });
    }

})();
