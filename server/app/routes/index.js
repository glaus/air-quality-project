/**
 * For Routes which use the Auth middleware a Bearer Token must be set.
 * @type {*|exports|module.exports}
 */


var express = require('express');
var router = express.Router();
var multer  = require('multer');

var indexController = require('../controllers/index');
var authController = require('../controllers/auth.js');
var userController = require('../controllers/user');

var temperatureController = require('../controllers/temperature');
var coalGasController = require('../controllers/coalgas');
var lightLevelController = require('../controllers/lightlevel');
var dustController = require('../controllers/dust');
var formolGasController = require('../controllers/formolgas');
var humidityController = require('../controllers/humidity');
var co2Controller = require('../controllers/co2');

var auth = require('../middleware/auth.js');

/**
 * HTTP Routes
 */
router.get('/',indexController.home);
router.post('/auth/login', authController.login);

router.get('/user',auth,userController.list);
router.get('/user/:id',auth,userController.get);
router.post('/user',auth,userController.add);
router.put('/user/:id',auth,userController.update);
router.delete('/user/:id',auth,userController.delete);

router.get('/temperature',temperatureController.list);
router.get('/temperature/:id',temperatureController.get);
router.get('/temperature/latest',temperatureController.getLatest);
router.get('/temperature/statistics/hour',temperatureController.getStatisticHour);
router.get('/temperature/statistics/day',temperatureController.getStatisticDay);
router.post('/temperature',temperatureController.add);
router.post('/temperature/:value',temperatureController.addParam);
router.put('/temperature/:id',temperatureController.update);
router.delete('/temperature/:id',temperatureController.delete);

router.get('/coalgas',coalGasController.list);
router.get('/coalgas/:id',coalGasController.get);
router.get('/coalgas/latest',coalGasController.getLatest);
router.get('/coalgas/statistics/hour',coalGasController.getStatisticHour);
router.get('/coalgas/statistics/day',coalGasController.getStatisticDay);
router.post('/coalgas',coalGasController.add);
router.post('/coalgas/:value',coalGasController.addParam);
router.put('/coalgas/:id',coalGasController.update);
router.delete('/coalgas/:id',coalGasController.delete);

router.get('/formolgas',formolGasController.list);
router.get('/formolgas/:id',formolGasController.get);
router.get('/formolgas/latest',formolGasController.getLatest);
router.get('/formolgas/statistics/hour',formolGasController.getStatisticHour);
router.get('/formolgas/statistics/day',formolGasController.getStatisticDay);
router.post('/formolgas',formolGasController.add);
router.post('/formolgas/:value',formolGasController.addParam);
router.put('/formolgas/:id',formolGasController.update);
router.delete('/formolgas/:id',formolGasController.delete);

router.get('/humidity',humidityController.list);
router.get('/humidity/:id',humidityController.get);
router.get('/humidity/latest',humidityController.getLatest);
router.get('/humidity/statistics/hour',humidityController.getStatisticHour);
router.get('/humidity/statistics/day',humidityController.getStatisticDay);
router.post('/humidity',humidityController.add);
router.post('/humidity/:value',humidityController.addParam);
router.put('/humidity/:id',humidityController.update);
router.delete('/humidity/:id',humidityController.delete);

router.get('/dust',dustController.list);
router.get('/dust/:id',dustController.get);
router.get('/dust/latest',dustController.getLatest);
router.get('/dust/statistics/hour',dustController.getStatisticHour);
router.get('/dust/statistics/day',dustController.getStatisticDay);
router.post('/dust',dustController.add);
router.post('/dust/:value',dustController.addParam);
router.put('/dust/:id',dustController.update);
router.delete('/dust/:id',dustController.delete);

router.get('/lightlevel',lightLevelController.list);
router.get('/lightlevel/:id',lightLevelController.get);
router.get('/lightlevel/latest',lightLevelController.getLatest);
router.get('/lightlevel/statistics/hour',lightLevelController.getStatisticHour);
router.get('/lightlevel/statistics/day',lightLevelController.getStatisticDay);
router.post('/lightlevel',lightLevelController.add);
router.post('/lightlevel/:value',lightLevelController.addParam);
router.put('/lightlevel/:id',lightLevelController.update);
router.delete('/lightlevel/:id',lightLevelController.delete);

router.get('/co2',co2Controller.list);
router.get('/co2/:id',co2Controller.get);
router.get('/co2/latest',co2Controller.getLatest);
router.get('/co2/statistics/hour',co2Controller.getStatisticHour);
router.get('/co2/statistics/day',co2Controller.getStatisticDay);
router.post('/co2',co2Controller.add);
router.post('/co2/:value',co2Controller.addParam);
router.put('/co2/:id',co2Controller.update);
router.delete('/co2/:id',co2Controller.delete);

router.get('/time', indexController.time);

module.exports = router;
