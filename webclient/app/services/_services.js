(function() {
    "use strict";

    angular
        .module("mt.services", [
            "mt.services.api"
        ]);
})();
