(function() {
    "use strict";

    angular
        .module("mt.controller.co2",[])
        .config(usecaseRouteProvider);

    function usecaseRouteProvider($stateProvider) {
        $stateProvider
            .state("co2", {
                views: {
                    "mt-content" : {
                        templateUrl: "co2.jade",
                        controller: "Co2Controller"
                    },
                    "mt-topbar":{
                        templateUrl: "topbar.jade",
                        controller:"TopbarController"
                    }
                },
                parent:"mt",
                url:"/co2"
            });
    }

})();
