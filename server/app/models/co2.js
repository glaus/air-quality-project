'use strict';
module.exports = function(sequelize, DataTypes) {
    var Temperature = sequelize.define('CO2', {
        value: {
            type: DataTypes.INTEGER
        }
    }, {
        freezeTableName: true,
        paranoid: false,
        classMethods: {
            associate: function(models) {

            }
        }
    });
    return Temperature;
};