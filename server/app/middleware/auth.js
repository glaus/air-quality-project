var jwt = require('jsonwebtoken');
var config = require('../config/config.js');

var auth = function(req, res, next) {
    var token = req.body.token || req.query.token || req.headers['authorization'];
    if (token) {
        token = token.slice(7,token.length);
        jwt.verify(token, config.secret, function(err, decoded) {
            if (err) {
                return res.status(403).send({ success: false, message: 'Failed to authenticate token.' });
            } else {
                // if everything is good, save to request for use in other routes
                req.decoded = decoded;
                next();
            }
        });

    } else {
        noTokenProvided(res)
    }
};

function noTokenProvided(res){
    return res.status(403).send({
        success: false,
        message: 'No token provided.'
    });
}

module.exports = auth;
