


(function() {
    "use strict";

    angular
        .module("mt.controller.formolgasDay",[])
        .config(usecaseRouteProvider);

    function usecaseRouteProvider($stateProvider) {
        $stateProvider
            .state("formolgasDay", {
                views: {
                    "mt-content" : {
                        templateUrl: "formolgasDay.jade",
                        controller: "FormolgasDayController"
                    },
                    "mt-topbar":{
                        templateUrl: "topbar.jade",
                        controller:"TopbarController"
                    }
                },
                parent:"mt",
                url:"/formolgas/day"
            });
    }

})();
