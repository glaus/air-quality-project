(function() {
    "use strict";

    angular
        .module("mt.services.api.humidity")
        .factory("HumidityLatest", ["$resource","Config",usecaseService])
        .factory("HumidityStatistics",["$resource","Config",tempStatistik])
        .factory("HumidityStatisticsDay",["$resource","Config",tempStatistikDay]);

    /**
     * Auth service, handles simple login
     */
    function usecaseService($resource,Config) {
        return $resource(Config.apiUrl+'/humidity/latest',{},{
            'get':{
                method:'GET'
            }
        });
    }

    function tempStatistik($resource,Config) {
        return $resource(Config.apiUrl+'/humidity/statistics/hour',{},{
            'get':{
                method:'GET',
                isArray:true
            }
        });
    }

    function tempStatistikDay($resource,Config) {
        return $resource(Config.apiUrl+'/humidity/statistics/day',{},{
            'get':{
                method:'GET',
                isArray:true
            }
        });
    }

})();