(function() {
  "use strict";

  angular
    .module("mt", [
      //libs
        "ui.router",
        "ngMessages",
        "ngResource",
        "ngCookies",
        "chart.js",

        //MT Modules
        "mt.templates",
        "mt.controller",
        "mt.services"
    ]);
})();
