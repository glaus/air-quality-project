(function() {
  "use strict";

  angular
    .module("mt.services.api", [
        "mt.services.api.temperature",
        "mt.services.api.lightlevel",
        "mt.services.api.coalgas",
        "mt.services.api.dust",
        "mt.services.api.humidity",
        "mt.services.api.formolgas",
        "mt.services.api.co2"
    ]);

})();
