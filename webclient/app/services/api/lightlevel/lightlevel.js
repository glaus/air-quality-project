(function() {
    "use strict";

    angular
        .module("mt.services.api.lightlevel")
        .factory("LightLevelLatest", ["$resource","Config",usecaseService])
        .factory("LightLevelStatistics",["$resource","Config",tempStatistik])
        .factory("LightLevelStatisticsDay",["$resource","Config",tempStatistikDay]);

    /**
     * Auth service, handles simple login
     */
    function usecaseService($resource,Config) {
        return $resource(Config.apiUrl+'/lightlevel/latest',{},{
            'get':{
                method:'GET'
            }
        });
    }

    function tempStatistik($resource,Config) {
        return $resource(Config.apiUrl+'/lightlevel/statistics/hour',{},{
            'get':{
                method:'GET',
                isArray:true
            }
        });
    }

    function tempStatistikDay($resource,Config) {
        return $resource(Config.apiUrl+'/lightlevel/statistics/day',{},{
            'get':{
                method:'GET',
                isArray:true
            }
        });
    }

})();