(function() {
    "use strict";

    angular
        .module("mt.controller.humidityDay",[])
        .config(usecaseRouteProvider);

    function usecaseRouteProvider($stateProvider) {
        $stateProvider
            .state("humidityDay", {
                views: {
                    "mt-content" : {
                        templateUrl: "humidityDay.jade",
                        controller: "HumidityDayController"
                    },
                    "mt-topbar":{
                        templateUrl: "topbar.jade",
                        controller:"TopbarController"
                    }
                },
                parent:"mt",
                url:"/humidity/day"
            });
    }

})();
