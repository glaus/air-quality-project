'use strict';
module.exports = function(sequelize, DataTypes) {
    var Temperature = sequelize.define('Humidity', {
        value: {
            type: DataTypes.FLOAT
        }
    }, {
        freezeTableName: true,
        paranoid: false,
        classMethods: {
            associate: function(models) {

            }
        }
    });
    return Temperature;
};