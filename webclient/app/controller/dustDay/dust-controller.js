(function() {
    "use strict";

    angular.module("mt.controller.dustDay")
        .controller("DustDayController", ["$scope","$state","$interval","DustStatisticsDay",overviewController]);

    function overviewController($scope, $state,$interval,DustStatistics) {
        getStatsTemp();

        $scope.statsValues = [];
        $scope.statsKeys = [];
        $scope.labels = $scope.statsKeys;
        $scope.series = ['Series B'];
        $scope.data = [
            $scope.statsValues
        ];
        $scope.onClick = function (points, evt) {
            console.log(points, evt);
        };
        $scope.datasetOverride = [{ yAxisID: 'y-axis-1' }];
        $scope.options = {
            scales: {
                yAxes: [
                    {
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left',
                        ticks: {
                            beginAtZero: true,
                            steps: 10,
                            stepValue: 100,
                            max: 1000,
                            min: 0
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'Dust pcs/0.01 cf'
                        }
                    }
                ]
            }
        };

        function getStatsTemp(){
            DustStatistics.get({}, function(res){
                console.log(res);
                for (var i = 0; i < res.length; i++) {
                    console.log(res[i]);
                    $scope.statsValues.push(res[i].val);
                    $scope.statsKeys.push(res[i].hour+":00");
                }
                console.log($scope.statsValues);
                console.log($scope.statsKeys);
            },function(err){
                console.log(err)
            });
        }
    }

})();
