(function() {
    "use strict";

    angular
        .module("mt.controller.dustDay",[])
        .config(usecaseRouteProvider);

    function usecaseRouteProvider($stateProvider) {
        $stateProvider
            .state("dustDay", {
                views: {
                    "mt-content" : {
                        templateUrl: "dustDay.jade",
                        controller: "DustDayController"
                    },
                    "mt-topbar":{
                        templateUrl: "topbar.jade",
                        controller:"TopbarController"
                    }
                },
                parent:"mt",
                url:"/dust/day"
            });
    }

})();
