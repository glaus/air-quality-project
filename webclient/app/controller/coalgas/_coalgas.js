(function() {
    "use strict";

    angular
        .module("mt.controller.coalgas",[])
        .config(usecaseRouteProvider);

    function usecaseRouteProvider($stateProvider) {
        $stateProvider
            .state("coalgas", {
                views: {
                    "mt-content" : {
                        templateUrl: "coalgas.jade",
                        controller: "CoalGasController"
                    },
                    "mt-topbar":{
                        templateUrl: "topbar.jade",
                        controller:"TopbarController"
                    }
                },
                parent:"mt",
                url:"/coalgas"
            });
    }

})();
