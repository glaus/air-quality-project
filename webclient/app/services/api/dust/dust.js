(function() {
    "use strict";

    angular
        .module("mt.services.api.dust")
        .factory("DustLatest", ["$resource","Config",usecaseService])
        .factory("DustStatistics",["$resource","Config",tempStatistik])
        .factory("DustStatisticsDay",["$resource","Config",tempStatistikDay]);

    /**
     * Auth service, handles simple login
     */
    function usecaseService($resource,Config) {
        return $resource(Config.apiUrl+'/dust/latest',{},{
            'get':{
                method:'GET'
            }
        });
    }

    function tempStatistik($resource,Config) {
        return $resource(Config.apiUrl+'/dust/statistics/hour',{},{
            'get':{
                method:'GET',
                isArray:true
            }
        });
    }

    function tempStatistikDay($resource,Config) {
        return $resource(Config.apiUrl+'/dust/statistics/day',{},{
            'get':{
                method:'GET',
                isArray:true
            }
        });
    }

})();